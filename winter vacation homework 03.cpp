#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void help (void)
{
	printf ("帮助信息:\n");
	printf ("您需要输入命令代号来进行操作，且\n");
	printf ("一年级题目为不超过十位的加减法:\n");
	printf ("二年级题目为不超过百位的乘除法:\n");
	printf ("三年级题目为不超过百位的加减乘除混合题目.\n");
	printf ("\n");
}

void menu (void)
{
	printf ("操作列表:\n");
	printf ("1)一年级    2)二年级    3)三年级\n");
	printf ("4)帮助      5)退出程序\n");
	printf ("请输入操作> ");
}

void execute (void)
{
	printf ("< 执行操作 :)\n");
	printf ("\n");
}

void error (void)
{
	printf ("Error!!!\n");
	printf ("错误操作指令，请重新输入\n");
	printf ("\n");
}

int grade1 ()
{
	int i, a;
	char op;
	srand((unsigned)time(NULL));
	printf ("现在是一年级题目:\n");
	printf ("请输入生成个数> ");
	scanf ("%d", &a);
	for (i=1;i<=a;i++)
	{
		int x, y;
		
		x=rand();
		y=rand();
		op=rand();
		while (x>=10)
		{
			x=x/10;
		}
		while (y>=10)
		{
			y=y/10;
		}
		if (op>45)
		{
			while (op!=45)
			{
				op--;
			}
		}
		else
		{
			while (op!=43)
			{
				op++;
			}
		}
		printf ("%d %c %d = ___\n", x, op, y);
	}
	printf ("执行完了\n\n");
}

int grade2 ()
{
	int i, a;
	char op;
	srand((unsigned)time(NULL));
	printf ("现在是二年级题目:\n");
	printf ("请输入生成个数> ");
	scanf ("%d", &a);
	for (i=1;i<=a;i++)
	{
		int x, y;
		
		x=rand();
		y=rand();
		op=rand();
		while (x>=100)
		{
			x=x/10;
		}
		while (y>=100)
		{
			y=y/10;
		}
		if (op>47)
		{
			while (op!=47)
			{
				op--;
			}
		}
		else
		{
			while (op!=42)
			{
				op++;
			}
		}
		printf ("%d %c %d = ___\n", x, op, y);
	}
	printf ("执行完了\n\n");
}

int grade3 ()
{
	int i, a;
	srand((unsigned)time(NULL));
	printf ("现在是三年级题目:\n");
	printf ("请输入生成个数> ");
	scanf ("%d", &a);
	for (i=1;i<=a;)
	{
		int x, y, z;
		char op1, op2;
		x=rand();
		y=rand();
		z=rand();
		op1=rand()%6+42;
		op2=rand()%6+42;
		while (x>=100)
		{
			x=x/10;
		}
		while (y>=100)
		{
			y=y/10;
		}
		while (z>=100)
		{
			z=z/10;
		}
		if (op1==44||op1==46||op2==44||op2==46)
			continue;
		if (op1==47&&y==0||op2==47&&y==0)
			continue;
			else
				printf ("%d %c %d %c %d = ___\n", x, op1, y, op2, z);
				i++;
	}
	printf ("执行完了\n\n");
}

int answer (int n)
{
	switch (n) {
		case 1: grade1 (); break;
		case 2: grade2 (); break;
		case 3: grade3 (); break;
		case 4: help (); break;
		case 5: printf ("程序结束，欢迎下次使用，按任意键结束......"); break;
	}
}

int main (void)
{
	int n;
	printf ("=======口算生成器=======\n");
	printf ("欢迎使用口算生成器 :)\n");
	printf ("\n");
	help ();
	menu ();
	scanf ("%d", &n);
	execute ();
	while (n!=5)
	{
		answer (n);
		menu ();
		scanf ("%d", &n);
		execute ();
		if (n>5||n<1)
			error ();
	}
	answer (5);
	
	return 0;
}
