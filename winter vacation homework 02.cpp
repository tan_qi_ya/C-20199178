#include<stdio.h> 
void help (void)
{
	printf ("帮助信息:\n");
	printf ("您需要输入命令代号来进行操作，且\n");
	printf ("一年级题目为不超过十位的加减法:\n");
	printf ("二年级题目为不超过百位的乘除法:\n");
	printf ("三年级题目为不超过百位的加减乘除混合题目.\n");
	printf ("\n");
}

void menu (void)
{
	printf ("操作列表:\n");
	printf ("1)一年级    2)二年级    3)三年级\n");
	printf ("4)帮助      5)退出程序\n");
	printf ("请输入操作> ");
}

void execute (void)
{
	printf ("< 执行操作 :)\n");
	printf ("\n");
}

void error (void)
{
	printf ("Error!!!\n");
	printf ("错误操作指令，请重新输入\n");
	printf ("\n");
}

int grade (int n)
{
	switch (n) {
		case 1: printf ("现在是一年级题目:\n执行完了\n\n"); break;
		case 2: printf ("现在是二年级题目:\n执行完了\n\n"); break;
		case 3: printf ("现在是三年级题目:\n执行完了\n\n"); break;
		case 4: help (); break;
		case 5: printf ("程序结束，欢迎下次使用，按任意键结束......"); break;
	}
}

int main (void)
{
	int n;
	printf ("=======口算生成器=======\n");
	printf ("欢迎使用口算生成器 :)\n");
	printf ("\n");
	help ();
	menu ();
	scanf ("%d", &n);
	execute ();
	while (n!=5)
	{
		grade (n);
		menu ();
		scanf ("%d", &n);
		execute ();
		if (n>5||n<1)
			error ();
	}
	grade (5);
	
	return 0;
}
